# .NET Developer Learning Path

## Milestone 1: Get Started
- [ ] Build .NET applications with C#
  - 6 modules (3 hr 14 min)
- [ ] Build web apps with ASP.NET Core for beginners
  - 6 modules (3 hr 40 min)

## Milestone 2: Web Apps
- [ ] Build your first web app with Blazor (32 min)
- [ ] Create a web UI with ASP.NET Core (45 min)

## Milestone 3: Backend APIs & Microservices
- [ ] Create web apps and services with ASP.NET Core, minimal API, and .NET
  - 3 modules (1 hr 35 min)
- [ ] Create cloud-native apps and services with .NET and ASP.NET Core
  - 7 modules (4 hr 25 min)
- [ ] Create a web API with ASP.NET Core controllers (41 min)

## Milestone 4: Mobile & Desktop Apps
- [ ] Build mobile and desktop apps with .NET MAUI
  - 9 modules (7 hr 18 min)

## Milestone 5: Links
- Placeholder untuk sumber daya tambahan dan link.

## Milestone 6: Learn C#
- [ ] Add logic to C# console applications (Get started with C#, Part 3)
  - 7 modules (6 hr 21 min) - In progress
- [ ] Work with variable data in C# console applications (Get started with C#, Part 4)
  - 7 modules (6 hr 21 min)
- [ ] Create and run simple C# console applications (Get started with C#, Part 2)
  - 7 modules (6 hr 33 min) - In progress
- [ ] Write your first code using C# (Get started with C#, Part 1)
  - 6 modules (4 hr 37 min) - Completed
- [ ] Create methods in C# console applications (Get started with C#, Part 5)
  - 5 modules (3 hr 55 min)
- [ ] Debug C# console applications (Get started with C#, Part 6)
  - 6 modules (5 hr 27 min)

## Milestone 7: Cloud Development
- [ ] AZ-204: Implement Azure Functions
  - 2 modules (51 min)
- [ ] Migrate ASP.NET Apps to Azure
  - 4 modules (3 hr 37 min)
- [ ] Transform your business applications with fusion development
  - 5 modules (3 hr 11 min)
- [ ] Explore Microsoft Graph scenarios for ASP.NET Core development
  - 3 modules (1 hr 48 min)
- [ ] Migrate an ASP.NET web application to Azure with Visual Studio
  - 2 modules (53 min)
